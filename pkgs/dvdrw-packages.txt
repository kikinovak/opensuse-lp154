###############################
# DVD/RW-related applications #
###############################

# Asunder
asunder
asunder-lang

# HandBrake
handbrake-gtk
handbrake-gtk-lang

# K3B
k3b
k3b-lang

